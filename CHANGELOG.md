## 1.2.3 (2023-06-14)
### Fixed (1 change)
- fix #2 Description is always set to '0' (@prplecake)

## 1.2.2 (2022-01-10)
### Fixed (1 change)
- fix a bug when updating the addon where undefined tags and list were added as default tag/list.

## 1.2.0 (2022-01-11)
### add (1 change)
- add new features support for linkace 1.8 ; user can now add default lists and tags.

## 1.1.0 (2021-12-21)

### Fixed (1 change)
- Automatically close window once the link is bookmarked.

